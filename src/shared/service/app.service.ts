import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import { Hand } from '../model/hand';

const URL_PREFIX = 'http://localhost:82/v1/';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,PATCH,DELETE,HEAD,OPTIONS',
        //'Remote Address': 'localhost:4200',
        //'Sec-Fetch-Mode': 'navigate',
        //'Sec-Fetch-Site': 'none'
    })
};

@Injectable({providedIn: 'root'})
export class AppService {
    constructor(private httpClient: HttpClient) {}

    /**
     * 
     * @returns HttpHeaders
     */
    public getHeaders() {
        const headers = new HttpHeaders();

        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE,HEAD,OPTIONS');
        headers.append('Remote Address', 'localhost:4200');
        headers.append('Sec-Fetch-Mode', 'navigate');
        headers.append('Sec-Fetch-Site', 'none');
        return headers;
    }

    /**
     * 
     * @returns 
     */
    public generateRandomSortRuleGroups() {
        let headers = this.getHeaders();
        return this.httpClient.get(URL_PREFIX + 'rules', httpOptions).pipe(
            map(data => data),
        );
    }

    /**
     * 
     * @returns 
     */
    public generateRandomHand() {
        let headers = this.getHeaders();
        return this.httpClient.get(URL_PREFIX + 'hand', httpOptions).pipe(
            map(hand => new Hand(hand))
        );
    }

    /**
     * 
     * @returns 
     */
    public sortHand(cards, sortRuleGroups) {
        let headers = this.getHeaders();
        let body = {
            cards : cards,
            sortRuleGroups : sortRuleGroups
        }
        return this.httpClient.post(URL_PREFIX + 'cards/sort', body, httpOptions).pipe(
            map(cards => {
                return cards
            })
        );
    }
}