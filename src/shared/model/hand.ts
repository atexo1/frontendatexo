import { Card } from "./card";

export class Hand {
    sortedByValueRules: string[];
    sortedByColorRules: string[];
    cards: Card[];
    sortedCards: Card[];

    constructor(data = null) {
        this.cards = data?.cards;
        this.sortedCards = data?.sortedCards;
        this.sortedByValueRules = data?.sortedByValueRules;
        this.sortedByColorRules = data?.sortedByColorRules;
    }
}