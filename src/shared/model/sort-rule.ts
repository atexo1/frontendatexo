import { Hand } from "./hand";

export class SortRule {
    item: string;
    weight: number;

    constructor(data = null) {
        this.item = data?.item;
        this.weight = data?.weight;
    }
}