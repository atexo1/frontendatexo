import { SortRule } from "./sort-rule";

export class SortRuleGroup {
    field: string;
    coefficient: string;
    sortRules: SortRule[];

    constructor(data = null) {
        this.field = data?.field;
        this.coefficient = data?.coefficient;
        this.sortRules = data?.sortRules;
    }
}