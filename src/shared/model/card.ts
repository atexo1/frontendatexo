import { Hand } from "./hand";

export class Card {
    value: string;
    color: string;
    weight: number;
    //hand?: Hand = null;


    constructor(data = null) {
        this.value = data?.value;
        this.color = data?.color;
        this.weight = data?.weight;
    }
}