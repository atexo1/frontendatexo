import { Component } from '@angular/core';
import {
  faUser,
  faChevronLeft,
  faChevronRight,
  faBan,
} from '@fortawesome/free-solid-svg-icons';
import { Card } from 'src/shared/model/card';
import { Hand } from 'src/shared/model/hand';
import { SortRuleGroup } from 'src/shared/model/sort-rule-group';
import { AppService } from 'src/shared/service/app.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent {

  hands : Hand[] = [];
  hand : Hand = null;
  sortRuleGroups : SortRuleGroup[] = [];
  sortRules : {
    sortByValueRules : string[],
    sortByColorRules : string[]
  } = null;
  sortRuleGroupByColor : SortRuleGroup;
  sortRuleGroupByValue : SortRuleGroup;
  sortedCards : Card[] = [];


  step : number = 1;

  faPerson = faUser;
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  faBan = faBan;

  constructor(private appService : AppService) {}

  generateSortRules() {
    this.appService.generateRandomSortRuleGroups().subscribe(sortRuleGroups => {
      this.sortRuleGroups = sortRuleGroups as SortRuleGroup[];
      this.sortRuleGroupByColor = this.sortRuleGroups.find(item => item.field === 'color')
      this.sortRuleGroupByValue = this.sortRuleGroups.find(item => item.field === 'value')
      
    });
  }

  generateRandomHand() {
    this.step = 2;
    this.appService.generateRandomHand().subscribe(hand => {
      this.hand = hand as Hand;      
    });
  }

  sortRandomHand() {
    this.step = 3;
    this.appService.sortHand(this.hand.cards, this.sortRuleGroups).subscribe(sortedCards => {
      this.sortedCards = sortedCards as Card[];      
    });
  }

  goToFirstStep() {
    this.step = 1;
    this.hand = null;
  }

  goToSecondStep() {
    this.step = 2;
    this.sortedCards = null;
  }

  initAll() {
    this.step = 1;
    this.sortRules = null;

    this.sortRuleGroups = null;
    this.sortRuleGroupByColor = null
    this.sortRuleGroupByValue = null

    this.hand = null;
    this.sortedCards = null;
  }
}
